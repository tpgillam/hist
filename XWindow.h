#ifndef LESTER_XWINDOW_H
#define LESTER_XWINDOW_H

#include <cairo/cairo.h>
#include <ostream>

struct XWindow {
  void start();
  virtual ~XWindow() {};
  virtual void paint(cairo_t * c, std::ostream & os) const = 0;
};

#endif 
