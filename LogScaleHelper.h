
#ifndef LOGSCALEHELPER_H
#define LOGSCALEHELPER_H

class Hist;

void findLargestAndSmallestWeightToPlot(
  // inputs:
  const bool forY, // true=y, false=z
  const double hist_maxWeight,
  const double hist_minWeight,
  const double hist_smallestPosWeight,
// outputs:
  double & smallestWeightToPlot,
  double & largestWeightToPlot);

void findLargestAndSmallestWeightToPlot(
  // inputs:
  const bool forY, // true=y, false=z
  const Hist & hist,
  // outputs:
  double & smallestWeightToPlot,
  double & largestWeightToPlot);

#endif

