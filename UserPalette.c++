
#include "UserPalette.h"

std::shared_ptr<UserPalette> UserPalette::instance() {
  static std::shared_ptr<UserPalette> instance;
  if (!(instance.get())) {
    instance = std::shared_ptr<UserPalette>(new UserPalette);
  }
  return instance;
}
