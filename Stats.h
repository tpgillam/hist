
#ifndef LESTER_STATS_H
#define LESTER_STATS_H

#include <iostream>

class Stats {
public:
  Stats() : n(0) {
  }
  void note(const double x, const double weight=1) {
    /*
      const unsigned long newN = n+1;
      const double newMean = (mean*n + x)/(n+1);
      const double newVariance = 
      n==0 
      ? 0 
      : variance*(n-1)/n  + sq(x-newMean)/n + sq((x-mean)/(n+1));
      
      n        = newN;
      mean     = newMean;
      variance = newVariance;
    */

    // Algorithm below from Wikipedia: Algorithms_for_calculating_variance
    if (n==0) {
      n = 1;
      mean = x;
      S = 0;
      sumweight = weight;
    } else {
      n = n + 1;
      const double temp = weight + sumweight;
      S = S + sumweight*weight*sq(x-mean) / temp;
      mean = mean + (x-mean)*weight / temp;
      sumweight = temp;
    }
  }
  double getN() const { return n; }
  double getMean() const { return mean; }
  double getSampleVariance() const { return S * n / ((n-1) * sumweight); }
  double getVariance() const { return S / sumweight; }
  double getRMS() const;
private:
  inline double sq(const double x) const { return x*x; }
  unsigned long n;
  double mean;
  //double variance;
  double S;
  double sumweight;
};

inline std::ostream & operator<<(std::ostream & os, const Stats & stats) {
  return os << "[Mean " << stats.getMean() << ", RMS " << stats.getRMS() << "]"; 
}

#endif
