
#ifndef LESTER_TFMHISTBOX_H
#define LESTER_TFMHISTBOX_H

#include "CairoPaintable.h"

class TfmHistBox : public CairoPaintable {
 public:
  TfmHistBox(double lx, double ly,
	     double ux, double uy) : lx(lx), ly(ly), ux(ux), uy(uy) {}
  void paint(cairo_t * c, std::ostream & os) const;
 private:
  float lx;
  float ly;
  float ux;
  float uy;
};

#endif
