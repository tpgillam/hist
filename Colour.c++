
#include "Colour.h"
#include "Configuration.h"
#include <cassert>

void Colour::paint(cairo_t * c, std::ostream & os) const {
  if (c) {
    cairo_set_source_rgb(c, red,green,blue);
  }
}

void Colour::moveTowards(const Colour & other) {
  red = (red+other.red)*0.5;
  green = (green+other.green)*0.5;
  blue = (blue+other.blue)*0.5;
}

void Colour::paint(cairo_t * c, std::ostream & os, const double gamma) const {
  if (c) {
    cairo_set_source_rgba(c, red,green,blue,gamma);
  }
}

Colour Colour::setColourFromIndex(unsigned int colour) {
  if (colour==0) {
      if (!Configuration::dark) {
          // Normal white background, so
          return Colour(0,0,0); // black foreground
      } else {
          // black background in dark mode, so
          return Colour(1,1,1); // white foreground
      }
  }
  colour -= 1;
  double intense=1;
  const double shrink=0.7;
  while (colour>=7) {
    colour -= 7;
    intense *= shrink;
  }
  switch (colour) {
  case 0:
    return Colour(intense, 0, 0);
    break;
  case 1:
    return Colour(0, 0, intense);
    break;
  case 2:
    return Colour(0, intense, 0);
    break;
  case 3:
    return Colour(0, intense, intense);
    break;
  case 4:
    return Colour(intense*0.75, intense*0.75, 0); // Make the yellow a bit dirtier so that it shows up better.
    break;
  case 5:
    return Colour(intense, 0, intense);
    break;
  case 6:
    return Colour(1-intense*shrink, 1-intense*shrink, 1-intense*shrink);
    break;
  }
  assert(false);
}


