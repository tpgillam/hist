
#ifndef LESTER_NONINVALIDATINGSEQUENCE_H
#define LESTER_NONINVALIDATINGSEQUENCE_H

#include <memory>
#include <vector>
#include <list>

//#include <iostream>

  // We need a container that we can safely read the "old" parts of
  // (i.e. without any iterator invalidation) while another thread
  // simultaneously builds on the end.

  // Within the STL the only suitable class is "list", though this 
  // does more than we need.  Since it supports constant time insertion
  // and deletion at the middle of the list, it has to store two pointers
  // in addition to each piece of payload, which is wasteful of
  // memory for our purposes.

  // The class NonInvalidatingSequence tries to do something closer
  // to what we want, by being an object which we can "push_back" to,
  // and which never invalidates old iterators when we do so, but which
  // doesn't need to store two pointers per piece of payload.

  // NonInvalidatingSequence might have bugs in it, however, and does
  // not implement a genuine STL-type interface, so if in doubt use
  // good old std::list.

  // However, experimentation seems to show that for 1D hist operation
  // you can save a factor 3 in memory (40% of memory filled when
  // 25,000,000 doubles have been histogrammed with NonInvalidatingSequence
  // versus 40% of memory filled when 8,335,258 doubles have been
  // histogrammed using std::list.  My test system has 506736 kB memory
  // and reports sizeof(double)=8, so the figures above are consistent
  // with the intention that memory usage for NonInvalidatingSequence
  // is >99% payload, and <1% pointers.
  // Since sizeof(float)=4, a further doubling in capacity could be 
  // achieved by moving to floats.

  // There is a small (<10%) spped peanalty associated with
  // NonInvalidatingSequence ... probably because I do not manage
  // the iterators very well


template <class T>
class NonInvalidatingSequence {
public:
  class const_iterator; // fwd dec
  NonInvalidatingSequence(const unsigned long defaultStartCapacity=1024) : m_startCapacity((defaultStartCapacity>0)?defaultStartCapacity:1024) , m_size(0) {
  }
  void push_back(const T & t) {
    if (m_listOfVecPtrs.empty()) {
      // currently empty
      std::shared_ptr<std::vector<T> > newVec(new std::vector<T>());
      newVec->reserve(m_startCapacity);
      newVec->push_back(t);
      m_listOfVecPtrs.push_back(newVec);
      //std::mes() << "made first vec and pushed " << t << " on back" << std::endl; 
    } else {
      // not empty, so add to existing vec if space, otherwise allocate new one. and add to that ...
      std::shared_ptr<std::vector<T> > & lastVec = m_listOfVecPtrs.back();
      if (lastVec->size() < lastVec->capacity()) {
	// there is room to add there:
	lastVec->push_back(t);
	//std::mes() << "re-used current vec and pushed " << t << " on back" << std::endl; 
      } else {
	// no room left, create new vector
	std::shared_ptr<std::vector<T> > newVec(new std::vector<T>());
	newVec->reserve(2*(lastVec->capacity()));
	newVec->push_back(t);
	m_listOfVecPtrs.push_back(newVec);
	//std::mes() << "made new vec (last one filled) and pushed " << t << " on back" << std::endl; 
      }
    }
    ++m_size;
  }
  unsigned long size() const {
	return m_size;
  }
  const_iterator begin() const {
    if (m_listOfVecPtrs.empty() || m_listOfVecPtrs.front()->empty()) {
      return end();
    }
    const_iterator beginIt(m_listOfVecPtrs.begin(), m_listOfVecPtrs.front()->begin(), false, &m_listOfVecPtrs);
    return beginIt;
  }
  const_iterator end() const {
    const_iterator endIt;
    return endIt;
  }
  class const_iterator {
    friend class NonInvalidatingSequence<T>;
  private:
    const_iterator(const typename std::list<std::shared_ptr<std::vector<T> > >::const_iterator & listIt,
		   const typename std::vector<T>::const_iterator & vecIt,
		   const bool pastTheEnd,
		   const typename std::list<std::shared_ptr<std::vector<T> > > * const listOfVecPtrsPtr) 
      : m_listIt(listIt), m_vecIt(vecIt), m_pastTheEnd(pastTheEnd), m_listOfVecPtrsPtr(listOfVecPtrsPtr) {
    }
  public:
    const_iterator() : m_pastTheEnd(true) {}
    const T & operator*() const { return *m_vecIt; }
    const T * operator->() const { return m_vecIt.operator->(); }
    bool operator!=(const const_iterator & other) const {
      return !(operator==(other));
    }
    bool operator==(const const_iterator & other) const {
      if (m_pastTheEnd != other.m_pastTheEnd) {
	return false;
      }
      if (m_pastTheEnd == true) {
	return true;
      }
      // so neither says m_pastTheEnd.
      return 
	m_listIt == other.m_listIt &&
	m_vecIt  == other.m_vecIt;
    }
    void operator++() {
      if (m_pastTheEnd) return;
      ++m_vecIt;
      if ( m_vecIt == (*m_listIt)->end() ) {
	++m_listIt;
	if ( m_listIt == m_listOfVecPtrsPtr->end()) {
	  m_pastTheEnd = true;
	} else {
	  m_vecIt = (*m_listIt)->begin();
	  if (m_vecIt == (*m_listIt)->end()) {
	    m_pastTheEnd = true;
	  }   
	}
      }
    }
      
  private:
    typename std::list<std::shared_ptr<std::vector<T> > >::const_iterator m_listIt;
    typename std::vector<T>::const_iterator m_vecIt;
    bool m_pastTheEnd;
    const typename std::list<std::shared_ptr<std::vector<T> > > * m_listOfVecPtrsPtr;
  };
private:
  std::list<std::shared_ptr<std::vector<T> > > m_listOfVecPtrs;
  const unsigned long m_startCapacity;
  unsigned long m_size;
};

#endif
