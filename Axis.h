
#ifndef LESTER_AXIS_H
#define LESTER_AXIS_H

#include "Bin.h"
#include <cmath>
#include <iostream>
#include <cassert>

struct Axis {
  Axis(const unsigned long _n, const double _from, const double _to, const double isLogScale, const int isIntAxis) :
    isLogScale(isLogScale), isIntAxis(isIntAxis) {
    if (isIntAxis) {
      from = floor(_from);
      to = ceil(_to);
      if (to<from) {
        std::cerr << "Silly bin range specification: from=" << from <<", to=" << to<< std::endl;
        throw "SillyBins";
      }
      n = to - from + 1;
    } else {
      if (_to<=_from || _n==0) {
        std::cerr << "Silly bin range specification: from=" << from <<", to=" << to<< ", n=" << n << std::endl;
        throw "SillyBins";
      }
      from = _from;
      to = _to;
      n = _n;
    }
  }
  double leftMostExtent() const {
    return getBinByNumber(0).from;
  }
  double rightMostExtent() const {
    assert(n>=1);
    return getBinByNumber(n-1).to;
  }
  unsigned int n; // numberOfBins
  private:
  double from;
  double to;
  public:
  bool isLogScale;
  bool isIntAxis; // If true, then from and to are taken to be integers representing the lower and upper integr bin CENRES for integer bins, and n is ignored;
  Bin getBin(const double x) const {
    if (isIntAxis) {
      const int binN = static_cast<int>(floorl(x-from+0.5));
      return getBinByNumber(binN);
    } else {
      const int binN=static_cast<int>(floorl((x-from)*n/(to-from)));
      return getBinByNumber(binN);
    }
  }
  Bin getBinByNumber(const int binN) const {
    if (isIntAxis) {
      return Bin(from + binN - 0.5 ,
                 from + binN + 0.5 );
    } else {
      return Bin( from + ((to-from)/n)*(binN+0),
                  from + ((to-from)/n)*(binN+1)  );
    }
  }
  bool operator!=(const Axis & other) const {
    return !((*this) == other);
  }
  bool operator==(const Axis & other) const {
    return other.n==n && other.from==from && other.to==to;
  }
};

#endif


