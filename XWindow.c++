
#include "XWindow.h"

//#include <cairo/cairo-pdf.h>
//#include <cairo/cairo-ps.h>
#include <cairo/cairo-xlib.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include "TheBuffer.h"
#include "Configuration.h"
#include "Synch.h"
#include <cmath>
#include <unistd.h>

void XWindow::start() {
  //try {
  Display *dpy;
  Window rootwin;
  Window win;
  XEvent e;
  int scr;
  cairo_surface_t *cs = NULL;

  if (!(dpy = XOpenDisplay(NULL))) {
    std::mes() << "ERROR: Could not open display" << std::endl;
    exit(1);
  }

  scr = DefaultScreen(dpy);
  rootwin = RootWindow(dpy, scr);

  win = XCreateSimpleWindow(dpy, rootwin, 1, 1, Configuration::SIZEX,
                            Configuration::SIZEY, 0, BlackPixel(dpy, scr),
                            BlackPixel(dpy, scr));

  XStoreName(dpy, win, "Lester Hist");
  XSelectInput(dpy, win, ExposureMask | ButtonPressMask | StructureNotifyMask | KeyPressMask);
  XMapWindow(dpy, win);

  //std::mes() << "XWindow about to call wait" << std::endl;
  Synch::waitForXWindowThreadReadyBeforeProceeding();
  //std::mes() << "XWindow finished calling wait" << std::endl;

  /// NON BLOCKING EVENT LOOP:
  const unsigned int bufsiz = 255;
  char text[bufsiz]; // a char buffer for KeyPress wvents.
  KeySym key; // something for handling KeyPress events.
  bool keepGoing = true;
  //std::mes() << "XWindow moo " << __LINE__ << std::endl;
  while (keepGoing) {
    bool needRedraw = false;
    while (XPending(dpy)) {
      //std::mes() << "XWindow moo " << __LINE__ << std::endl;
      XNextEvent(dpy, &e);
      if (e.type == Expose && e.xexpose.count < 1) {
        needRedraw = true;
      } else if (e.type == ButtonPress) {
        // keepGoing=false; // INSERT THIS LINE IF YOU WANT TO CLOSE ON A MOUSE
        // CLICK!
      } else if (e.type == KeyPress&&
          //XLookupString(&e.xkey,text,bufsiz,&key,0)==1) {
          XLookupString(&e.xkey,text,bufsiz,&key,0)==1) { // XLookupString seems to return a number of characters used in the text buf.  By testing it for 1 we check that it contains one character (I think).
        /* use the XLookupString routine to convert the invent
         * KeyPress data into regular text.  Weird but necessary...
         */
        if (text[0]=='q') {
           keepGoing = false; // should really use something like this, but sadly that won't interrup the input stream and cause an orderly dump of files, etc.  So instead:
           exit(1);
        }
        //std::mes() << "XWindow moo " << __LINE__ << std::endl;
      } else if (e.type == ConfigureNotify) {
        //std::mes() << "XWindow moo " << __LINE__ << std::endl;
        const XConfigureEvent &xce = e.xconfigure;
        //std::cout << xce.width << std::endl;
        //std::cout << xce.height << std::endl;
        const double normW = xce.width / 525.0;
        const double normH = xce.height / 500.0;
        int newW, newH;
        if (normW < normH) {
          // The height is tooo big for the width we have. Set using width
          newW = xce.width;
          newH = ceil(xce.width * 500.0 / 525.0);
        } else {
          newW = ceil(xce.height * 525.0 / 500.0);
          newH = xce.height;
        }

        Configuration::SIZEX = newW;
        Configuration::SIZEY = newH;
        if (cs != NULL) {
          cairo_surface_destroy(cs);
          cs = NULL;
        }
        cs = cairo_xlib_surface_create(dpy, win, DefaultVisual(dpy, 0),
                                       Configuration::SIZEX,
                                       Configuration::SIZEY);
        needRedraw = true;
      }
    }

    static bool first = true;
    static TheBuffer::BufferType lastBuffer = TheBuffer::instance().get();
    TheBuffer::BufferType currentBuffer = TheBuffer::instance().get();
    if (currentBuffer != lastBuffer ||
        first) {  // technically not right as a new buffer might co-incidentally
                  // be in the same location as an old buffer?
      needRedraw = true;
      lastBuffer = currentBuffer;
      first = false;
    }

    if (needRedraw) {
      //std::mes() << "Need redraw!" << std::endl;

      cairo_t *c = cairo_create(cs);
      //std::mes() << "Need redraw a!" << std::endl;
      this->paint(c, std::cerr);
      //std::mes() << "Need redraw b!" << std::endl;
      cairo_show_page(c);
      //std::mes() << "Need redraw c!" << std::endl;
      cairo_destroy(c);
      //std::mes() << "Need redraw d!" << std::endl;

    } else {
      //std::mes() << "Don't need redraw" << std::endl;
      usleep(100000);  // microseconds
    }
  }

  if (cs != NULL) {
    cairo_surface_destroy(cs);
  }
  XCloseDisplay(dpy);
 // } catch (...) {
 //    std::cout << "Something funny" << std::endl;
 //    while (true) {
 //       usleep (10000);
 //    }  
 // }
}

