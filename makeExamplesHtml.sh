#!/bin/sh
cat EXAMPLES | \

gawk '

     /^expo/ { print "<td><img src=",$2,"/></td>" }  
     /^rm/   { print "<td><pre>" ,$0, "</pre></td>" }
     /^$/    { print "</tr><tr>" }

     BEGIN   { print "<html><body><table><tr>" }
     END     { print "</tr></table></body></html>" }

' | \
sed 's/ FILE=\(.*png\)/"\1"/g' | \
sed 's/|/| \\\n/g' | \
cat > /usera/lester/public_html/hist/examplesFragment.html

cp *.png /usera/lester/public_html/hist/
chmod a+r /usera/lester/public_html/hist/*.png
