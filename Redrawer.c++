
// Test

#include "Redrawer.h"
#include <thread>
#include "Data.h"
#include "TheBuffer.h"
#include "Scene.h"
#include "Background.h"
#include "Paintable1DHistogram.h"
#include "Paintable1DRatioHistogram.h"
#include "Paintable2DHistogram.h"
#include "Configuration.h"
#include <iostream>
#include <sstream>
#include "Hist1D.h"
#include "Hist2D.h"
#include "Synch.h"
#include <unistd.h>

/// Universally, for "redraw" read "buffer build" to get the philosophy of this class.

Redrawer::Redrawer() : m_everBuilt(false) {
}

void Redrawer::handleRedrawsEtc() {
  // Redraws is a bit of a mis-nomer.  BufferBuilder would be better, as the job of this class is to keep the buffers up to date at a reasonable rate.
  Synch::waitForRedrawerThreadReadyBeforeProceeding();

  this->bufferBuild();
  while(true) {
    usleep(1000000); // no need to build the buffer more than once per second (argument is microseconds)
    unsigned long currentDataSize;
    {
      std::lock_guard<std::mutex> lock(Data::mutex);
      currentDataSize = Data::size;
    }
    const bool thereIsNewData = currentDataSize > m_lastBuilt;
    if (!thereIsNewData) {
      continue;
    }
    // There is some new data.  Is it worth building the buffer?

    // Yes if there is a significant amount of new data!
    if (currentDataSize >= m_nextInterestingDataSize) {
      //std::mes() << "REDRAW BECAUSE " << currentDataSize << " >= " << m_nextInterestingDataSize  << std::endl;
      this->bufferBuild();
      continue;
    }

    // Ok, there wasn't a significant amount of new data.  Maybe we should build anyway if we are bored:
    const time_t now = time(0);
    const double dif = difftime(now, m_lastBuildTime);
    if (dif>Configuration::boredomTime) {
      //std::mes() << "REDRAW BECAUSE BORED" << std::endl;
      this->bufferBuild();
      continue;
    }

    // we got here ... dont need a rebuild.  Just "go around"
    continue;
  }
}

inline std::string statString(const unsigned long currentDataSize, const Stats & xStats, const Stats & yStats) {
  std::ostringstream os;
  os << currentDataSize << " points";
  os << ", X:["<<Configuration::nx << " bins from "
     <<Configuration::lx << " to " <<Configuration::ux
     << ", Mean="<<xStats.getMean()
     << ", RMS="<<xStats.getRMS()
     << "]";
  if (Configuration::dim >= 2) {
    os << ", Y:["<<Configuration::ny << " bins from "
       <<Configuration::ly << " to " <<Configuration::uy
       << ", Mean="<<yStats.getMean()
       << ", RMS="<<yStats.getRMS()
       << "]";
  }
  return os.str();
}

void Redrawer::doStats() const {
  BoundMonitor xMonitor, yMonitor;
  Stats xStats, yStats;
  xMonitor = Data::xMonitor;
  yMonitor = Data::yMonitor;
  Configuration::update(Data::size, Data::xMonitor, Data::yMonitor);
  const std::string theStatString = statString(Data::size, Data::xStats, Data::yStats);
  std::mes() << theStatString << std::endl;
}

typedef std::map<std::string, unsigned int> SizeMap;
template <class T>
SizeMap getSizeMap(T t) {
  SizeMap ans;
#ifdef DEBUG
  unsigned long tot = 0;
#endif
  for (typename T::const_iterator it = t.begin();
       it != t.end();
       ++it) {
    const std::string kind = it->first;
    const unsigned long size = it->second->size();
#ifdef DEBUG
    tot += size;
#endif
    ans.insert(SizeMap::value_type(kind, size));
  }
#ifdef DEBUG
  assert(tot==0 || tot==Data::size);
#endif
  return ans;
}

template <class T>
std::map<std::string, std::shared_ptr<T> > deepCopy(std::map<std::string, std::shared_ptr<T> > histPtrMap, const bool normalize) {
  typedef std::shared_ptr<T> Ptr;
  typedef std::map<std::string, Ptr> Map;
  Map ans;
  for (typename Map::const_iterator it = histPtrMap.begin();
       it != histPtrMap.end();
       ++it) {
    const std::string & kind = it->first;
    Ptr old = it->second;
    Ptr deep(new T(*old));  // a real copy, not copying the pointer!
    if (Configuration::cumulate) {
        std::cout << "Cumulating" << std::endl;
      deep->cumulate(); // includes normalization
    } else if (normalize && !(Configuration::cumulate)) {
      deep->normalize();
    }
    ans.insert(typename Map::value_type(kind, deep));
  }
  return ans;
};

void Redrawer::bufferBuild() {
  // never want more than one simultaneous attempt at a buffer build, because of the way we preserve state at the end of this method ...
  std::lock_guard<std::mutex> lk(this->m_mutex);

  {
    std::lock_guard<std::mutex> lock(Data::mutex);
    // Unless currentDataSize is zero (in which case this might be the first time we have been asked to build the buffer) there CANNOT be a reason to build a buffer if there is no new data so:
    // If there is no data at all, it is still worth drawing the axes so that the user can see something.
    if (m_everBuilt && !(Data::size>m_lastBuilt)) {
      return; // ps ... this is only legitimate because of our use of this->m_mutex at the top of this method, and the fact that all processing outside this mutexed scope does not involve calls to Data::
    }
  }

  std::shared_ptr<Scene> scene(new Scene);
  scene->list().push_back(TheBuffer::BufferType(new Background()));


  unsigned long size_built;



  if (Configuration::autosizing) {

    // Hold a lock just long enough to get ACTUAL map copies.  Note, this fixes the KINDS we will work with, but doesn't stop more data arriving for those kinds between now and when we actually plot the data ... so we have to save the current sizes so as not to get different kinds out of synch with each other.

    // NB, the following code is a bit wasteful, as really only the appropriate ONE of the following four types really needs to be copied.  However the other three will be trivial copies, so we don't worry about the performance hit.  It will not be noticeable.

    //////////////////////////////// LOCK /////////////////////////////////
    Data::mutex.lock();
    Data::Kinds                current_kinds                = Data::kinds;
    Data::Data1DPtrMap         current_data1DPtrMap         = Data::data1DPtrMap;
    Data::Data1DWeightedPtrMap current_data1DWeightedPtrMap = Data::data1DWeightedPtrMap;
    Data::Data2DPtrMap         current_data2DPtrMap         = Data::data2DPtrMap;
    Data::Data2DWeightedPtrMap current_data2DWeightedPtrMap = Data::data2DWeightedPtrMap;
    SizeMap sizeMap_1D         = getSizeMap(current_data1DPtrMap        );
    SizeMap sizeMap_1DWeighted = getSizeMap(current_data1DWeightedPtrMap);
    SizeMap sizeMap_2D         = getSizeMap(current_data2DPtrMap        );
    SizeMap sizeMap_2DWeighted = getSizeMap(current_data2DWeightedPtrMap);
    doStats();
    size_built = Data::size;
    Data::mutex.unlock();
    /////////////////////////////// UN-LOCK ////////////////////////////////

    if (Configuration::dim==1) {
      // First, make enough histograms to store all the data ...
      Data::Hist1DPtrMap histPtrMap;

      for (Data::Kinds::const_iterator it = current_kinds.begin();
           it != current_kinds.end();
           ++it) {
        const std::string kind = *it;

        std::shared_ptr<Hist1D> histPtr(new Hist1D(Axis(Configuration::nx,
                                          Configuration::lx,
                                          Configuration::ux, false, Configuration::intx)));
        Hist1D & hist = *(histPtr); // convenience
        histPtrMap.insert(Data::Hist1DPtrMap::value_type(kind, histPtr));

        if (Configuration::weighted) {
          // weighted
          Data::Data1DWeighted & data1DWeighted = *(current_data1DWeightedPtrMap[kind]);
          Data::Data1DWeighted::const_iterator it = data1DWeighted.begin();
          const unsigned long size = sizeMap_1DWeighted[kind];
          if (Configuration::overwrite) {
            for (unsigned long i=0; i<size; (++it,++i)) {
              if (Configuration::ignoreweights) {
                hist.fillOverwrite(it->x);
              } else {
                hist.fillOverwrite(it->x, it->w);
              }
            }
          } else {
            for (unsigned long i=0; i<size; (++it,++i)) {
              hist.fill(it->x, it->w);
            }
          }
        } else {
          // not weighted
          Data::Data1D & data1D = *(current_data1DPtrMap[kind]);
          Data::Data1D::const_iterator it = data1D.begin();
          const unsigned long size = sizeMap_1D[kind];
          if (Configuration::overwrite) {
            for (unsigned long i=0; i<size; (++it,++i)) {
              hist.fillOverwrite(it->x);
            }
          } else {
            for (unsigned long i=0; i<size; (++it,++i)) {
              hist.fill(it->x);
            }
          }
        }
        if (Configuration::cumulate) {
          hist.cumulate();
        } else if (Configuration::normalize && !Configuration::cumulate) {
          hist.normalize();
        }
      }

      if (Configuration::ratio) {
        scene->list().push_back(TheBuffer::BufferType(new Paintable1DRatioHistogram(histPtrMap)));
      } else {
        scene->list().push_back(TheBuffer::BufferType(new Paintable1DHistogram(histPtrMap)));
      }
    } else if (Configuration::dim==2) {

      // First, make enough histograms to store all the data ...
      Data::Hist2DPtrMap histPtrMap;

      for (Data::Kinds::const_iterator it = current_kinds.begin();
           it != current_kinds.end();
           ++it) {
        const std::string kind = *it;
        std::shared_ptr<Hist2D> histPtr(new Hist2D(Axis(Configuration::nx,
                                          Configuration::lx,
                                          Configuration::ux, false, Configuration::intx),
                                          Axis(Configuration::ny,
                                               Configuration::ly,
                                               Configuration::uy, false, Configuration::inty)));
        Hist2D & hist = *(histPtr); // convenience
        histPtrMap.insert(Data::Hist2DPtrMap::value_type(kind, histPtr));

        if (Configuration::weighted) {
          // weighted
          Data::Data2DWeighted & data2DWeighted = *(current_data2DWeightedPtrMap[kind]);
          Data::Data2DWeighted::const_iterator it = data2DWeighted.begin();
          const unsigned long size = sizeMap_2DWeighted[kind];
          if (Configuration::overwrite) {
            for (unsigned long i=0; i<size; (++it,++i)) {
              if (Configuration::ignoreweights) {
                hist.fillOverwrite(it->x, it->y);
              } else {
                hist.fillOverwrite(it->x, it->y, it->w);
              }
            }
          } else {
            for (unsigned long i=0; i<size; (++it,++i)) {
              hist.fill(it->x, it->y, it->w);
            }
          }
        } else {
          // not weighted
          Data::Data2D & data2D = *(current_data2DPtrMap[kind]);
          Data::Data2D::const_iterator it = data2D.begin();
          const unsigned long size = sizeMap_2D[kind];
          if (Configuration::overwrite) {
            for (unsigned long i=0; i<size; (++it,++i)) {
              hist.fillOverwrite(it->x, it->y);
            }
          } else {
            for (unsigned long i=0; i<size; (++it,++i)) {
              hist.fill(it->x, it->y);
            }
          }
        }
        if (Configuration::cumulate) {
          hist.cumulate();
        } else if (Configuration::normalize && !Configuration::cumulate) {
          hist.normalize();
        }
      }
      scene->list().push_back(TheBuffer::BufferType(new Paintable2DHistogram(histPtrMap)));
    }
  } else {
    // Not autosizing, so:  Fast Mode


    // Hold a lock just long enough to get ACTUAL copies if each histogram (not pointer copies!)
    // Again, don't really need to copy BOTH 1D and 2D, but no-one will notice the insignificant performance hit in doing both:

    //////////////////////////////// LOCK /////////////////////////////////
    Data::mutex.lock();
    Data::Kinds                current_kinds                = Data::kinds;
    Data::Hist1DPtrMap current_1 = deepCopy(Data::hist1DPtrMap, Configuration::normalize);
    Data::Hist2DPtrMap current_2 = deepCopy(Data::hist2DPtrMap, Configuration::normalize);
    size_built = Data::size;
    doStats();
    Data::mutex.unlock();
    /////////////////////////////// UN-LOCK ////////////////////////////////

    if (Configuration::dim==1) {
      if (Configuration::ratio) {
        scene->list().push_back(TheBuffer::BufferType(new Paintable1DRatioHistogram(current_1))); // Could make Paintable1DRatioHistogram and Paintable1DHistogram the same thing (so that we don't need these if statements) and let them hold the ifs ... perhaps??
      } else {
        scene->list().push_back(TheBuffer::BufferType(new Paintable1DHistogram(current_1)));
      }
    } else {
      scene->list().push_back(TheBuffer::BufferType(new Paintable2DHistogram(current_2)));
    }
  }

  TheBuffer::instance().set(scene);
  //std::mes() << "         .... done redrawing data at " << currentDataSize << " points"<< std::endl;

  m_lastBuilt = size_built;
  m_nextInterestingDataSize = m_lastBuilt + 1 + static_cast<unsigned long>(static_cast<double>(m_lastBuilt) * 0.1);
  m_everBuilt=true;
  m_lastBuildTime = time(0);
}

