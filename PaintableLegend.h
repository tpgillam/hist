
#ifndef LESTER_PAINTABLELEGEND_H
#define LESTER_PAINTABLELEGEND_H

#include "CairoPaintable.h"
#include <string>
#include <vector>
#include "Colour.h"

class PaintableLegend : public CairoPaintable {
public:
  PaintableLegend(const std::vector<std::string> & types,
	          const std::vector<Colour> & colours) :
    m_types(types),
    m_colours(colours) {
  }
  void paint(cairo_t * c, std::ostream & os) const; 
private:
  const std::vector<std::string> m_types;
  const std::vector<Colour> & m_colours;
};

#endif
