
#ifndef LESTER_RAINBOWPALETTE_H
#define LESTER_RAINBOWPALETTE_H

#include "InterpolatedPalette.h"

class RainbowPalette : public InterpolatedPalette {
 public:
  RainbowPalette();
};

#endif
