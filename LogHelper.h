#ifndef LOG_HELPER_ADAPT_H
#define LOG_HELPER_ADAPT_H

inline double adapt(const double d) {
  if (Configuration::haveLogScaleY) {
    if(d<=0) {
      std::mes() << "Trying to plot log of negative weight! " << __FILE__ << " " << __LINE__ <<std::endl;
    }
    return log(d);
  } else {
    return d;
  }
}

inline double adapt(const double d, const double smallestWeightToPlot) {
  return (d<smallestWeightToPlot) ? adapt(smallestWeightToPlot) : adapt(d);
}

#endif
