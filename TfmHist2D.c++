
#include "TfmHist2D.h"
#include "Hist2D.h"

TfmHist2D::TfmHist2D(const Hist2D & hist) : m_tfmHistBox(hist.xAxis.leftMostExtent(),
							 hist.yAxis.leftMostExtent(),
							 hist.xAxis.rightMostExtent(),
							 hist.yAxis.rightMostExtent()) {
}

void TfmHist2D::paint(cairo_t * c, std::ostream & os) const {
  m_tfmHistBox.paint(c,os);
}
