
#include "TfmHistBox.h"
#include "Configuration.h"

void TfmHistBox::paint(cairo_t * c, std::ostream & os) const {
  if (c) {
  // reset to "default" coordinate system
  cairo_identity_matrix(c);

  
  // translate origin from top-left to bottom-left:
  cairo_translate(c,0,+Configuration::SIZEY);

  
  // reverse y direction so that it increases UP the page
  cairo_scale(c, +1, -1);

  
  // translate corner of histogram to desired loc
  cairo_translate(c,
		  Configuration::borderFracLeft*static_cast<float>(Configuration::SIZEX),
		  Configuration::borderFracBot *static_cast<float>(Configuration::SIZEY));
  
  // scale histogram to fill desired box
  cairo_scale(c,
	      static_cast<float>(Configuration::SIZEX)*(1.-Configuration::borderFracLeft-Configuration::borderFracRight)/(ux-lx),
	      static_cast<float>(Configuration::SIZEY)*(1.-Configuration::borderFracBot -Configuration::borderFracTop  )/(uy-ly));
  
  // correct for coordinates of histogram corner
  cairo_translate(c,
		  -lx,
		  -ly);

  // patch up line width etc
  cairo_set_line_width(c,0.01);
  cairo_set_font_size(c, 0.05);
  }
}
