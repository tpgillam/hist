
#ifndef LESTER_PAINTABLEAXES_H
#define LESTER_PAINTABLEAXES_H

#include "CairoPaintable.h"
#include "Axis.h"
#include <list>

class PaintableAxes : public CairoPaintable {
public:
  PaintableAxes(const Axis & xAxis, const Axis & yAxis) :
    m_xAxis(xAxis),
    m_yAxis(yAxis) {
  }
  void paint(cairo_t * c, std::ostream & os) const; 
 public:
  static std::list<double> placesToLabel(const Axis & axis, const bool subTicks=false); 
private:
  Axis m_xAxis;
  Axis m_yAxis;
};

#endif
