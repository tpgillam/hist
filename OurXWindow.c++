#include "OurXWindow.h"

//#include <iostream>
#include "TheBuffer.h"
#include "CairoPaintable.h"

void OurXWindow::paint(cairo_t * c, std::ostream &os) const {
  //std::mes() << "Repainting OurXWindow ... " << std::flush;
  TheBuffer::BufferType buf = TheBuffer::instance().get();
  if (buf.get()) {
    //std::mes() << "found something to draw ... " << std::flush;
    buf->paint(c,os);
  }
  //std::mes() << "done." << std::endl;
}

