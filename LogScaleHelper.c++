
#include "LogScaleHelper.h"
#include "Configuration.h"
#include "Hist.h"


void findLargestAndSmallestWeightToPlot(
  // inputs:
  const bool forY, // true=y, false=z
  const Hist & hist,
  // outputs:
  double & smallestWeightToPlot,
  double & largestWeightToPlot) {
  findLargestAndSmallestWeightToPlot(forY, hist.maxWeight, hist.minWeight, hist.smallestPosWeight, smallestWeightToPlot, largestWeightToPlot);
}

void findLargestAndSmallestWeightToPlot(
  // inputs:
  const bool forY, // true=y, false=z
  const double hist_maxWeight,
  const double hist_minWeight,
  const double hist_smallestPosWeight,
  // outputs:
  double & smallestWeightToPlot,
  double & largestWeightToPlot) {

  const bool ConfigurationCChaveLogScaleZ
    = forY
      ? Configuration::haveLogScaleY
      : Configuration::haveLogScaleZ;

  const double ConfigurationCClz
    = forY
      ? Configuration::ly
      : Configuration::lz;

  const double ConfigurationCCuz
    = (forY
       ? Configuration::uy
       : Configuration::uz);

  const double extra=2; // must take the y-scale down to a bit lower than the minimum weight if you want to tell the difference between log (0) and log (smallestWeight).

  // std::mes() << "forY " << forY << std::endl;
  //  std::mes() << "Configuration::uy " << Configuration::uy << std::endl;
  //  std::mes() << "Configuration::uz " << Configuration::uz << std::endl;
  //  std::mes() << "(forY?1234:200) " << (forY?1234:200)<< std::endl;
  //  std::mes() << "(forY?Configuration::uy:Configuration::uz) " <<(forY?Configuration::uy:Configuration::uz) << std::endl;
  //  std::mes() << "ConfigurationCCuz " << ConfigurationCCuz << std::endl;

  //  std::mes() << __FILE__ << " " << __LINE__ << " arrg " << ConfigurationCCuz << " should be 200 because it is " << (forY?1234:200) << std::endl;

  //  std::mes() << __FILE__ << " " << __LINE__ << " arrg " << Configuration::uz << " should be 200 too" << std::endl;
//
//   std::mes() << __FILE__ << " " << __LINE__ << " arrg " << forY << " should be 0" << std::endl;

  const bool ConfigurationCCcalclz
    = forY
      ? Configuration::calcly
      : Configuration::calclz;

  const bool ConfigurationCCcalcuz
    = forY
      ? Configuration::calcuy
      : Configuration::calcuz;

  //  std::mes() << __FILE__ << " " << __LINE__ << " calclz " << Configuration::calclz << std::endl;

  if (ConfigurationCChaveLogScaleZ) {
//   std::mes() << __FILE__ << " " << __LINE__ << std::endl;

    // Log Scale Z !!
    // What is smallest weight to plot?
    // Start out with some defaults that are at least plottable, then correct them.
    smallestWeightToPlot = 0.1;
    largestWeightToPlot = 1;
    if (ConfigurationCCcalcuz==true && ConfigurationCCcalclz==true) {
      // auto both top and bot
      if (hist_maxWeight>0) {
        largestWeightToPlot=hist_maxWeight;
      }
      if (hist_smallestPosWeight>0 && hist_smallestPosWeight<hist_maxWeight) {
        smallestWeightToPlot = hist_smallestPosWeight/extra;
      } else {
        smallestWeightToPlot = largestWeightToPlot/10;
      }
    } else if (ConfigurationCCcalcuz==true) {
      // auto just top, using supplied bot
      if (ConfigurationCClz>0) {
        smallestWeightToPlot = ConfigurationCClz; // otherwise stick with default.
      }
      if (hist_maxWeight>smallestWeightToPlot) {
        largestWeightToPlot = hist_maxWeight;
      } else {
        largestWeightToPlot = smallestWeightToPlot*10;
      }
      //  std::mes() << __FILE__ << " " << __LINE__ << std::endl;
    } else if (ConfigurationCCcalcuz==true) {
      // auto just bot, using supplied top
      if (ConfigurationCCuz>0) {
        largestWeightToPlot = ConfigurationCCuz; // otherwise stick with default.
      }
      if (hist_smallestPosWeight>0 && hist_smallestPosWeight<largestWeightToPlot) {
        smallestWeightToPlot = hist_smallestPosWeight/extra;
      } else {
        smallestWeightToPlot = largestWeightToPlot/10;
      }
//   std::mes() << __FILE__ << " " << __LINE__ << std::endl;
    } else {
      // use supplied values for both top and bot,
      // but check they are valid.  If not valid, continue to use default.
      if (ConfigurationCCuz>ConfigurationCClz && ConfigurationCClz>0) {
        largestWeightToPlot=ConfigurationCCuz;
        smallestWeightToPlot=ConfigurationCClz;
      }
      //  std::mes() << __FILE__ << " " << __LINE__ << std::endl;
    }
//   std::mes() << __FILE__ << " " << __LINE__ << std::endl;


  } else {
    //  std::mes() << __FILE__ << " " << __LINE__ << std::endl;
    // Linear Scale Z !!

    // Start out with some defaults that are at least plottable, then correct them.
    smallestWeightToPlot = 0;
    largestWeightToPlot = 1;
//   std::mes() << __FILE__ << " " << __LINE__ << std::endl;

    if (ConfigurationCCcalcuz==true && ConfigurationCCcalclz==true) {
      //  std::mes() << __FILE__ << " " << __LINE__ << std::endl;
      // auto both top and bot
      largestWeightToPlot = hist_maxWeight;
      smallestWeightToPlot = hist_minWeight;
      if (hist_maxWeight == hist_minWeight) {
        // fix up bad range
        if (hist_maxWeight>0) {
          smallestWeightToPlot = 0;
        } else {
          largestWeightToPlot = 1;
        }
      }
    } else if (ConfigurationCCcalcuz==true) {
      //  std::mes() << __FILE__ << " " << __LINE__ << std::endl;
      // auto just top, using supplied bot
      smallestWeightToPlot = ConfigurationCClz;
      if (hist_maxWeight>smallestWeightToPlot) {
        largestWeightToPlot = hist_maxWeight;
      } else {
        largestWeightToPlot = smallestWeightToPlot+1;
      }
    } else if (ConfigurationCCcalclz==true) {
//   std::mes() << __FILE__ << " " << __LINE__ << std::endl;
      // auto just bot, using supplied top
      largestWeightToPlot = ConfigurationCCuz;
      if (hist_minWeight<largestWeightToPlot) {
        if (hist_minWeight<0) {
          smallestWeightToPlot = hist_minWeight;
        } else {
          smallestWeightToPlot=0;
        }
      } else {
        if (largestWeightToPlot>0) {
          smallestWeightToPlot=0;
        } else {
          smallestWeightToPlot = largestWeightToPlot-1;
        }
      }
    } else {
      //     std::mes() << __FILE__ << " " << __LINE__ << " uz " << ConfigurationCCuz << " lz " << ConfigurationCClz <<  " vs " << Configuration::uz << " lz " << Configuration::lz << " because " << forY << std::endl;
      // use supplied values for both top and bot,
      // but check they are valid.  If not valid, continue to use default.
      if (ConfigurationCCuz>ConfigurationCClz) {
        largestWeightToPlot=ConfigurationCCuz;
        smallestWeightToPlot=ConfigurationCClz;
      }
    }



  }

  // OK ... now smallest and largestWeightToPlot have been decided!
//   std::mes() << "MOO from " << smallestWeightToPlot << " to " << largestWeightToPlot << std::endl;
}


