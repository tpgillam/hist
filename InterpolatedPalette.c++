
#include "InterpolatedPalette.h"
#include "GreyscalePalette.h"

void InterpolatedPalette::setInterpolant(const double weight, const Colour & colour) {
  m_map[weight] = colour;
}


Colour InterpolatedPalette::getColourFrom(const double weight) const {

  if (m_map.empty()) {
    // there is no map, so return greyscale ... 
    static GreyscalePalette gsp;
    return gsp.getColourFrom(weight);
  }

  Map::const_iterator itWeightGEWeight = m_map.lower_bound(weight);
  const Map::const_iterator itWeightGTWeight = m_map.upper_bound(weight);
  
  if (itWeightGEWeight != itWeightGTWeight) {
    // the first's weight is equal to weight 
    return itWeightGEWeight->second;
  }
  
  // now know itWeightGEWeight==itWeightGTWeight, and both have a weight "greater" than wieght

  if (itWeightGEWeight == m_map.begin()) {
    // there is nowhere below us, so we saturate on the low colour;
    return m_map.begin()->second;
  }

  if (itWeightGEWeight == m_map.end()) {
    // there is nowhere above us, so we saturate in the high colour;
    return m_map.rbegin()->second;
  }

  // there is a colour below us:
  const Map::const_iterator & below = (--itWeightGEWeight);
  
  const double wBelow = below->first;
  const double wAbove = itWeightGTWeight->first;
  const Colour & cBelow = below->second;
  const Colour & cAbove = itWeightGTWeight->second;

  const double lambda = (weight-wBelow)/(wAbove-wBelow);
  const double mu = 1-lambda;
  
  return Colour(mu*cBelow.red   + lambda*cAbove.red  ,
		mu*cBelow.green + lambda*cAbove.green,
		mu*cBelow.blue  + lambda*cAbove.blue );
  
}
