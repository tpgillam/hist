
#ifndef LESTER_BACKGROUND_H
#define LESTER_BACKGROUND_H

#include "CairoPaintable.h"

// This class is badly named.  It should really be called 
// "Arbitrary square" or something similar.  It is just a
// testing class that is CairoPaintable.

class Background : public CairoPaintable {
 public:
  inline Background() : m_r(-1.f), m_g(-1.f), m_b(-1.f) {}
  inline Background(float r, float g, float b) : m_r(r), m_g(g), m_b(b) {}
  virtual void paint(cairo_t * c, std::ostream & os) const;
 private:
  float m_r;
  float m_g;
  float m_b;
};

#endif
