
#include "Paintable2DHistogram.h"
#include "TfmHist2D.h"
#include "PaintableAxes.h"
#include "PaintableLegend.h"
#include "PaintableTitle.h"
#include "ThePalette.h"
#include "LinearlyRemappedPalette.h"
#include "Configuration.h"
#include "ColourScale.h"
#include "Hist2D.h"
#include "LogScaleHelper.h"

const double transparencyLevel=0.5;

void Paintable2DHistogram::paint(cairo_t * c, std::ostream & os) const {

  if (m_hist2DPtrMap.empty()) {
    return;
  }

  // there is a m_hist2DPtrMap .... which maps names to histograms.  Most of the time we will simply want to plot each of those. However in the case of ratio mode (which implies multi) we will need to synthesize a new histogram out of two, and not plot the individuals.  Furthermore, if there is just one hisogram, then the name will be XXX and we need to plot just it, but without the multi-legends, etc.  So what we will do here is create a edited_hist2DPtrMap which looks like the m_hist2DPtrMap
  // except that any "generated" histograms -- such as the ratio histogram -- are found therein, in place of the things that they were built from. The idea is that then the drawer can take the histograms inside, and just draw them blindly. There is an exception, hwoever : since the ratio plotting style is a little different, there will still be some "if ratio" tests, or "if trueMulti" (where trueMulti will be "multi for the purposes of plotting, not multi for the purposes of data-entry
  
  const bool trueMulti = Configuration::multi && !(Configuration::ratio);
  Data::Hist2DPtrMap edited_hist2DPtrMap = m_hist2DPtrMap; // this will be overwritten if we are in ratio multi mode by the next few lines of code

////////////  Data::Hist2DPtr somePtr = (m_hist2DPtrMap.begin()->second); // would be sufficient in non-multi mode, but we might be in multi-mode, so:

  if (Configuration::multi && Configuration::ratio) {
      // we need to creae the edited_hist2DPtrMap map differently:
      
    const Data::Hist2DPtrMap::const_iterator aIt = m_hist2DPtrMap.find(Configuration::ratioTypeA);
    const Data::Hist2DPtrMap::const_iterator bIt = m_hist2DPtrMap.find(Configuration::ratioTypeB);
    if (aIt==m_hist2DPtrMap.end() || bIt==m_hist2DPtrMap.end()) {
      static bool first = true;
      if (first) {
        std::cerr << "You requested a ratio of kinds [" << Configuration::ratioTypeA << "] and [" << Configuration::ratioTypeB << "] but at least one of these types has yet to appear in the input stream.  So histo cannot yet be plotted.  Input stream presently contains " << m_hist2DPtrMap.size() << " kinds of data." << std::endl;
        first = false;
      }
      return;
    }

    try {
      const Data::Hist2DPtr ratioPtr(new Hist2D(Hist2D::divide(*(aIt->second), *(bIt->second)))); // need to copy
      edited_hist2DPtrMap = Data::Hist2DPtrMap(); // wipes the default constructor from above
      edited_hist2DPtrMap[Data::defaultKind] = ratioPtr; // fills with the one ratio thing that is needed

    } catch (...) {
      static bool first = true;
      if (first) {
        std::cerr << "The histograms you want me to make a ratio of are not compatible.   At time of writing, this error message probably indicates a fault somewhere in the program." << std::endl;
        first = false;
      }
      return;
    }
  }


  // Now loop over all histograms in the edited map:

  std::vector<Data::Kind> types;
  std::vector<Colour> colours;
  int nPlots = -1;
  for (Data::Hist2DPtrMap::const_iterator it = edited_hist2DPtrMap.begin();
          it != edited_hist2DPtrMap.end();
          ++it) {
      ++nPlots;
      const Data::Kind kind = (it->first);
      types.push_back(kind);
      int colour = 0;
      for (Data::Kinds::const_iterator jit = Data::kinds.begin();
              jit != Data::kinds.end();
              ++jit) {
          if ((*jit)==it->first) {
             break;
          }
          colour++;
      }
      const bool isFirstPlot = (nPlots==0);
      const Colour col = Colour::setColourFromIndex(colour);
      colours.push_back(col); // for legend
  const Hist2D & hist = *(it->second); // FIXME -- need better transparency, sometimes

  const bool needTransparency = trueMulti && edited_hist2DPtrMap.size()>1 && !(Configuration::overwrite) ;
  const bool needMultiColour = trueMulti && edited_hist2DPtrMap.size()>1 && (Configuration::overwrite) ;

  // If our palette has to adapt to data, then it is not until THIS point that we can be sure what mappings (if any) will have to be applied to our "basic" palette.
  /// START TO DETERMINE PALETTE
  std::shared_ptr<Palette> palette = ThePalette::get(); 


  double smallestWeightToPlot;
  double largestWeightToPlot;

  findLargestAndSmallestWeightToPlot(false, hist, smallestWeightToPlot, largestWeightToPlot);

  /*
  if (Configuration::haveLogScaleZ) {
    // Log Scale Z !!
    // What is smallest weight to plot?
    // Start out with some defaults that are at least plottable, then correct them.
    smallestWeightToPlot = 0.1;
    largestWeightToPlot = 1;
    if (Configuration::calcuz==true && Configuration::calclz==true) {
      // auto both top and bot
      if (hist.maxWeight>0) {
  largestWeightToPlot=hist.maxWeight;
      }
      if (hist.smallestPosWeight>0 && hist.smallestPosWeight<hist.maxWeight) {
  smallestWeightToPlot = hist.smallestPosWeight;
      } else {
  smallestWeightToPlot = largestWeightToPlot/10;
      }
    } else if (Configuration::calcuz==true) {
      // auto just top, using supplied bot
      if (Configuration::lz>0) {
  smallestWeightToPlot = Configuration::lz; // otherwise stick with default.
      }
      if (hist.maxWeight>smallestWeightToPlot) {
  largestWeightToPlot = hist.maxWeight;
      } else {
  largestWeightToPlot = smallestWeightToPlot*10;
      }
    } else if (Configuration::calcuz==true) {
      // auto just bot, using supplied top
       if (Configuration::uz>0) {
  largestWeightToPlot = Configuration::uz; // otherwise stick with default.
      }
       if (hist.smallestPosWeight>0 && hist.smallestPosWeight<largestWeightToPlot) {
  smallestWeightToPlot = hist.smallestPosWeight;
      } else {
  smallestWeightToPlot = largestWeightToPlot/10;
      }
    } else {
      // use supplied values for both top and bot,
      // but check they are valid.  If not valid, continue to use default.
      if (Configuration::uz>Configuration::lz && Configuration::lz>0) {
  largestWeightToPlot=Configuration::uz;
  smallestWeightToPlot=Configuration::lz;
      }
    }


  } else {
    // Linear Scale Z !!

    // Start out with some defaults that are at least plottable, then correct them.
    smallestWeightToPlot = 0;
    largestWeightToPlot = 1;

    if (Configuration::calcuz==true && Configuration::calclz==true) {
      // auto both top and bot
      largestWeightToPlot = hist.maxWeight;
      smallestWeightToPlot = hist.minWeight;
      if (hist.maxWeight == hist.minWeight) {
  // fix up bad range
  if (hist.maxWeight>0) {
    smallestWeightToPlot = 0;
  } else {
    largestWeightToPlot = 1;
  }
      }
    } else if (Configuration::calcuz==true) {
      // auto just top, using supplied bot
      smallestWeightToPlot = Configuration::lz;
      if (hist.maxWeight>smallestWeightToPlot) {
  largestWeightToPlot = hist.maxWeight;
      } else {
  largestWeightToPlot = smallestWeightToPlot+1;
      }
    } else if (Configuration::calcuz==true) {
      // auto just bot, using supplied top
      largestWeightToPlot = Configuration::uz;
      if (hist.minWeight<largestWeightToPlot) {
  if (hist.minWeight<0) {
    smallestWeightToPlot = hist.minWeight;
  } else {
    smallestWeightToPlot=0;
  }
      } else {
  if (largestWeightToPlot>0) {
    smallestWeightToPlot=0;
  } else {
    smallestWeightToPlot = largestWeightToPlot-1;
  }
      }
    } else {
      // use supplied values for both top and bot,
      // but check they are valid.  If not valid, continue to use default.
      if (Configuration::uz>Configuration::lz) {
  largestWeightToPlot=Configuration::uz;
  smallestWeightToPlot=Configuration::lz;
      }
    }



  }
  */
  // OK ... now smallest and largestWeightToPlot have been decided!



  // REMAP 0:1 palette to 0:maxWeight palette if desired


  if (Configuration::scaleWeightsToUnitIntervalWhenConvertingToColours) {

    if (Configuration::haveLogScaleZ) {
      // Log Z
      //std::cerr << "LOG rescaling palette" << std::endl;
      palette = std::shared_ptr<LinearlyRemappedPalette>(new LinearlyRemappedPalette(0, log(smallestWeightToPlot)/log(10.), 1, log(largestWeightToPlot)/log(10.), palette) );
    } else {
      // Linear z
      //std::cerr << "LIN rescaling palette" << std::endl;
      palette = std::shared_ptr<LinearlyRemappedPalette>(new LinearlyRemappedPalette(0, smallestWeightToPlot, 1, largestWeightToPlot, palette) );
    }
  } else {
    //std::cerr << "NOT rescaling palette" << std::endl;
  }


  TfmHist2D tfm(hist);
  tfm.paint(c,os);

  if (c) {

    if (Configuration::fillVoid && !Configuration::haveLogScaleZ) {
         // Blank the space under the canvas according to weight 0
         if (isFirstPlot) {
             ThePalette::get()->getColourFrom(0).paint(c,os);
             cairo_move_to(c, hist.xAxis.leftMostExtent(),  hist.yAxis.leftMostExtent());
             cairo_line_to(c, hist.xAxis.leftMostExtent(),  hist.yAxis.rightMostExtent());
             cairo_line_to(c, hist.xAxis.rightMostExtent(), hist.yAxis.rightMostExtent());
             cairo_line_to(c, hist.xAxis.rightMostExtent(), hist.yAxis.leftMostExtent());
             // implicit close! :)
             cairo_fill(c);
          }
    }
    if (Configuration::fillVoid && Configuration::haveLogScaleZ) {
      // Represent log(0)=-infty by red cross-hatching ...
      const int bits=50;
      for (int n=0; n<=bits; ++n) {
        const double lam=static_cast<double>(n)/static_cast<double>(bits);
        const double x1=lam*(hist.xAxis.leftMostExtent()) + (1.-lam)*(hist.xAxis.rightMostExtent());
        const double x2=lam*(hist.xAxis.rightMostExtent()) + (1.-lam)*(hist.xAxis.leftMostExtent());
        const double y1=lam*(hist.yAxis.leftMostExtent()) + (1.-lam)*(hist.yAxis.rightMostExtent());
        const double y2=lam*(hist.yAxis.rightMostExtent()) + (1.-lam)*(hist.yAxis.leftMostExtent());

        if (needTransparency) {
           Colour(1,0,0).paint(c,os,transparencyLevel); // red
        } else {
           Colour(1,0,0).paint(c,os); // red
        }
        cairo_move_to(c, x1, hist.yAxis.leftMostExtent());
        cairo_line_to(c,hist.xAxis.leftMostExtent() , y1);
        cairo_move_to(c, x1, hist.yAxis.leftMostExtent());
        cairo_line_to(c,hist.xAxis.rightMostExtent() , y2);

        if (n!=0 && n!=bits) {
          cairo_move_to(c, x2, hist.yAxis.rightMostExtent());
          cairo_line_to(c,hist.xAxis.rightMostExtent() , y2);
          cairo_move_to(c, x2, hist.yAxis.rightMostExtent());
          cairo_line_to(c,hist.xAxis.leftMostExtent() , y1);
        }
      }
      cairo_stroke(c); // I moved this one brace up ... I hope that is oK
    }
  }

  if (!c) {
    os << "#binXMin #binXMax #binYMin #binYMax #weight\n";
  }
  //cairo_set_source_rgb(c, 0.0, 0.0, .0);
  for (Hist2D::Data::const_iterator it = hist.data.begin();
       it != hist.data.end();
       ++it) {
    const Hist2D::Bin2D & bin2D = it->first;
    const Bin & xBin = bin2D.first;
    const Bin & yBin = bin2D.second;





    double weight = it->second;

    if (Configuration::haveLogScaleZ) {
      if (weight>0) {
        //static const onLog10=1./log(10.0);
        weight=log(weight)/log(10.);
      } else {
        // weight <=0 socan't take log!
        continue;
      }
    }


    if(c) {
      cairo_move_to(c,xBin.from,yBin.from);
      cairo_line_to(c,xBin.from,yBin.to);
      cairo_line_to(c,xBin.to,yBin.to);
      cairo_line_to(c,xBin.to,yBin.from);
      cairo_close_path(c);
      if (needTransparency) {
         Colour col2 = palette->getColourFrom(weight);
         col2.moveTowards(col);
         col2.paint(c,os,transparencyLevel);
      } else if (needMultiColour) {
         col.paint(c,os);
      } else {
         Colour col2 = palette->getColourFrom(weight);
         col2.paint(c,os);
      }
      //std::cerr << "Doing bin with col  for " << weight << " being " << palette->getColourFrom(weight) << std::endl;
      //cairo_set_source_rgb(c, weight, weight, weight);
      cairo_fill(c);
      //cairo_stroke(c);
    } else {
      // dump
      os << xBin.from << " " << xBin.to << " " << yBin.from << " " << yBin.to << " " << weight << "\n";
    }
  }


  // Now draw axes.
  PaintableAxes axes(hist.xAxis, hist.yAxis);
  axes.paint(c,os);

  // Now draw title.
  if (Configuration::title != "") {
    PaintableTitle title(Configuration::title);
    title.paint(c,os);
  }

  if (Configuration::colourScale) {

    //const double weightFrom = Configuration::calclz ? (Configuration::scaleWeightsToUnitIntervalWhenConvertingToColours ? 0 : hist.minWeight) : Configuration::lz;
    //const double weightTo   = Configuration::calcuz ? (Configuration::scaleWeightsToUnitIntervalWhenConvertingToColours ? 1 : hist.maxWeight) : Configuration::uz;

    //std::mes() << "WF " << weightFrom << " WT" << weightTo << std::endl;
    ColourScale colourScale(smallestWeightToPlot, largestWeightToPlot, Configuration::haveLogScaleZ, palette);
    colourScale.paint(c,os);
  }

  } // End of big loop over histograms (if multi)

  if (trueMulti) {
    // (6) Now draw legend.
    if (Configuration::showLegend) {
      PaintableLegend legend(types, colours);
      if (c) legend.paint(c,os);
    }
  }

  if (c) cairo_identity_matrix(c);



}



