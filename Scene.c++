#include "Scene.h"
//#include <iostream>

void Scene::paint(cairo_t * c, std::ostream & os) const {
  //std::mes() << "Painting a scene ... " << std::flush;
  for(CairoPaintableSharedPtrList::const_iterator it = m_list.begin();
      it != m_list.end();
      ++it) {
    //std::mes() << " item ... " << std::flush;
    (*it) -> paint(c,os);
  }
  //std::mes() << " scene ends. " << std::endl;
}
