
#ifndef LESTER_DATA_H
#define LESTER_DATA_H

#include <list>
#include <thread>
#include <map>
#include <set>
#include <mutex>
#include "BoundMonitor.h"
#include "Stats.h"
#include "NonInvalidatingSequence.h"

//fwd decs
class Hist1D;
class Hist2D;

struct Data {
  
  struct Point1D {
    Point1D(double x) : x(x) {}
    double x;
  };
    
  struct Point2D {
    Point2D(double x, double y) : x(x), y(y) {}
    double x;
    double y;
  };

  struct Point1DWeighted {
    Point1DWeighted(double x, double w) : x(x), w(w) {}
    double x;
    double w;
  };
    
  struct Point2DWeighted {
    Point2DWeighted(double x, double y, double w) : x(x), y(y), w(w) {}
    double x;
    double y;
    double w;
  };

  static BoundMonitor xMonitor;
  static BoundMonitor yMonitor;

  static Stats xStats;
  static Stats yStats;



  // We need a container that we can safely read the "old" parts of
  // (i.e. without any iterator invalidation) while another thread
  // simultaneously builds on the end.

  // Within the STL the only suitable class is "list", though this 
  // does more than we need.  Since it supports constant time insertion
  // and deletion at the middle of the list, it has to store two pointers
  // in addition to each piece of payload, which is wasteful of
  // memory for our purposes.

  // The class NonInvalidatingSequence tries to do something closer
  // to what we want, by being an object which we can "push_back" to,
  // and which never invalidates old iterators when we do so, but which
  // doesn't need to store two pointers per piece of payload.

  // NonInvalidatingSequence might have bugs in it, however, and does
  // not implement a genuine STL-type interface, so if in doubt use
  // good old std::list.

  // However, experimentation seems to show that for 1D hist operation
  // you can save a factor 3 in memory (40% of memory filled when
  // 25,000,000 doubles have been histogrammed with NonInvalidatingSequence
  // versus 40% of memory filled when 8,335,258 doubles have been
  // histogrammed using std::list.  My test system has 506736 kB memory
  // and reports sizeof(double)=8, so the figures above are consistent
  // with the intention that memory usage for NonInvalidatingSequence
  // is >99% payload, and <1% pointers.
  // Since sizeof(float)=4, a further doubling in capacity could be 
  // achieved by moving to floats.

  // There is a small (<10%) spped peanalty associated with
  // NonInvalidatingSequence ... probably because I do not manage
  // the iterators very well

//#define DATATYPE std::list
#define DATATYPE NonInvalidatingSequence

  typedef DATATYPE<Point1D>         Data1D;
  typedef DATATYPE<Point2D>         Data2D;
  typedef DATATYPE<Point1DWeighted> Data1DWeighted;
  typedef DATATYPE<Point2DWeighted> Data2DWeighted;
  typedef std::shared_ptr<Data1D>         Data1DPtr;
  typedef std::shared_ptr<Data2D>         Data2DPtr;
  typedef std::shared_ptr<Data1DWeighted> Data1DWeightedPtr;
  typedef std::shared_ptr<Data2DWeighted> Data2DWeightedPtr;
  typedef std::map<std::string, Data1DPtr>  Data1DPtrMap;
  typedef std::map<std::string, Data2DPtr>  Data2DPtrMap;
  typedef std::map<std::string, Data1DWeightedPtr>  Data1DWeightedPtrMap;
  typedef std::map<std::string, Data2DWeightedPtr>  Data2DWeightedPtrMap;

  typedef std::string  Kind;
  typedef std::vector<Kind>  Kinds;

  typedef std::shared_ptr<Hist1D> Hist1DPtr;
  typedef std::shared_ptr<Hist2D> Hist2DPtr;
  typedef std::map<Kind, Hist1DPtr> Hist1DPtrMap;
  typedef std::map<Kind, Hist2DPtr> Hist2DPtrMap;

  // If there are multiple types of data (sig, bg, etc, i.e. Configuration::multi==true) then data is stored in the relevant data map below, indexed by name opf type.
  // In this case, data1DPtr will be initially null, and then will point to the last filled data type.

  // If there is only one type of data (i.e. Configuration::multi==false) then the Maps are never used, but data1DPtr is null until filled.
  static Data1DPtr         data1DPtr;     // TODO: Move this object to main.c++ ... it shouldn't be global
  static Data2DPtr         data2DPtr;   // TODO: Move this object to main.c++ ... it shouldn't be global
  static Data1DWeightedPtr data1DWeightedPtr;
  static Data2DWeightedPtr data2DWeightedPtr;
  static Data1DPtrMap         data1DPtrMap;   // TODO: Move this object to main.c++ ... it shouldn't be global
  static Data2DPtrMap         data2DPtrMap;   // TODO: Move this object to main.c++ ... it shouldn't be global
  static Data1DWeightedPtrMap data1DWeightedPtrMap;
  static Data2DWeightedPtrMap data2DWeightedPtrMap;

  static Kinds kinds;
  static const Kind defaultKind;
 
  static Hist1DPtr hist1DPtr;
  static Hist2DPtr hist2DPtr;
  static Hist1DPtrMap hist1DPtrMap;
  static Hist2DPtrMap hist2DPtrMap;

  static unsigned long size;

  static bool hasBeenDrawn;
  static bool final;

  static std::mutex mutex;                // strong mutex: vetos messing with the data in any way
//  static std::mutex kindCreationMutex;    // lesser mutex: vetos messing with the data in any way that involves use of a new "kind". 
                    // Note: do not attempt to lock the ordinary mutex and the kindCreationMutex at the same time, or you may get a deadlock.

};

#endif
