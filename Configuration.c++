#include "Configuration.h"
#include "BoundMonitor.h"
#include <cmath>
#include <iostream>
#include <fstream>

bool Configuration::showHelp = false;
bool Configuration::normalize = false;
bool Configuration::silent = false;
bool Configuration::batch = false;
bool Configuration::multi = false;
bool Configuration::ratio = false;
bool Configuration::cumulate = false;
bool Configuration::ratioConnectPoints = true;
bool Configuration::showLegend = true;
std::string Configuration::ratioTypeA;
std::string Configuration::ratioTypeB;
unsigned int Configuration::dim = 1;
unsigned int Configuration::boredomTime = 60; // how long it takes to get bored in seconds
bool Configuration::calclx=true;
double Configuration::lx=0;
bool Configuration::calcux=true;
double Configuration::ux=1;
bool Configuration::calcly=true;
double Configuration::ly=0;
bool Configuration::calcuy=true;
double Configuration::uy=1;
bool Configuration::calclz=true;
double Configuration::lz=0;
bool Configuration::calcuz=true;
double Configuration::uz=1;
bool Configuration::calcnx=true;
unsigned int Configuration::nx=1;
bool Configuration::calcny=true;
unsigned int Configuration::ny=1;
unsigned int Configuration::SIZEX = 525;
unsigned int Configuration::SIZEY = 500;
float Configuration::borderFracLeft = 0.15;
float Configuration::borderFracRight = 0.20;
float Configuration::borderFracBot = 0.15;
float Configuration::borderFracTop = 0.15;
float Configuration::leftElbow = 0.05;
float Configuration::rightElbow = 0.05;
float Configuration::botElbow = 0.05;
float Configuration::topElbow = 0.05;
bool Configuration::weighted = false;
bool Configuration::coloured = false;
bool Configuration::autosizing = true;
bool Configuration::markLastPoint = false;
bool Configuration::makeEpsAtEnd = false;
std::string Configuration::epsFileName = "lesterHist.eps";
bool Configuration::makePdfAtEnd = false;
std::string Configuration::pdfFileName = "lesterHist.pdf";
bool Configuration::makeSvgAtEnd = false;
std::string Configuration::svgFileName = "lesterHist.svg";
bool Configuration::makePngAtEnd = false;
std::string Configuration::pngFileName = "lesterHist.png";
bool Configuration::makeDumpAtEnd = false;
std::string Configuration::dumpFileName = "lesterHist.dump";
std::string Configuration::xname;
std::string Configuration::yname;
bool Configuration::overwrite = false;
bool Configuration::ignoreweights = false;
bool Configuration::quitOnEnd = false;
bool Configuration::noBox = false;
std::string Configuration::title = "NewLesterHist";
//std::string Configuration::name = "newLesterHist";
bool Configuration::intx=false;
bool Configuration::inty=false;
bool Configuration::haveBoxX=false;
bool Configuration::haveBoxY=false;
double Configuration::boxx;
double Configuration::boxy;
bool Configuration::drawErrors=false;
bool Configuration::haveLogScaleY=false;
bool Configuration::haveLogScaleZ=false;
std::string Configuration::style;
double Configuration::graphThickness=0.003;
double Configuration::axesThickness=0.003;
bool Configuration::fillVoid=true;
bool Configuration::colourScale=false;
bool Configuration::scaleWeightsToUnitIntervalWhenConvertingToColours=true;
bool Configuration::centreJustify=true;
bool Configuration::dark=false;
Configuration::YAxisLabelDirection Configuration::yAxisLabelDirection = Configuration::UP;

void Configuration::fromName(const std::string & s, std::ostream & err) {
  if (s=="hist")           {
    hist(1,false);
  } else if (s=="hist2D")         {
    hist(2,false);
  } else if (s=="histWeighted")   {
    hist(1,true);
  } else if (s=="hist2DWeighted") {
    hist(2,true);
  } else {
    std::mes() << "Can't understand mode [" << s << "] so am going to assume plain [hist] mode instead!" << std::endl;
    hist(1,false);
  }
}

void Configuration::update(const unsigned long size, const BoundMonitor & xMonitor, const BoundMonitor & yMonitor) {

  if (calclx || calcux) {
    // calc x bounds
    if (true) {
      double w = 1;
      if (!(xMonitor.isSingular())) {
        w = xMonitor.width();
      }
      //std::mes() << "Wid = " << w << std::endl;
      if (calclx && calcux) {
        lx = xMonitor.min()-w*leftElbow;
        ux = xMonitor.max()+w*rightElbow;
      } else if (calclx) {
        lx = fmin(ux,xMonitor.min())-w*leftElbow; 
        // The fmin is needed to cope with the possibility that all events, thus far, are to the right of the supplied ux.
      } else if (calcux) {
        ux = fmax(lx,xMonitor.max())+w*rightElbow;
        // The fmax is needed to cope with the possibility that all events, thus far, are to the left of the supplied lx.
      }
      //std::mes() << "lx = " << lx << " ux=" << ux << std::endl;
    }
  }

  if (calcly || calcuy) {
    // calc y bounds
    if (true) {
      double w = 1;
      if (!(yMonitor.isSingular())) {
        w = yMonitor.width();
      }
      if (calcly && calcuy) {
        ly = yMonitor.min()-w*botElbow;
        uy = yMonitor.max()+w*topElbow;
      } else if (calcly) {
        ly = fmin(uy,yMonitor.min())-w*botElbow;
        // The fmin is needed to cope with the possibility that all events, thus far, are above the supplied uy.
      } else if (calcuy) {
        uy = fmax(ly,yMonitor.max())+w*topElbow;
        // The fmax is needed to cope with the possibility that all events, thus far, are below the supplied ly.
      }
    }
  }

  if (calcnx || calcny) {
    if (dim==1) {
      if (calcnx) {
        const double sugg = sqrt((double)(size));
        if (sugg>nx) {
          nx = (unsigned long) sugg;
        }
      }
    } else if (dim==2) {
      if (calcnx && calcny) {
        const double sugg = sqrt(sqrt((double)(size)));
        if (sugg>nx) {
          nx = (unsigned long) sugg;
        }
        if (sugg>ny) {
          ny = (unsigned long) sugg;
        }
      } else if (calcnx) {
        const double sugg = sqrt(((double)(size))/ny);
        if (sugg>nx) {
          nx = (unsigned long) sugg;
        }
      } else if (calcny) {
        const double sugg = sqrt(((double)(size))/nx);
        //std::mes() << "SUGG " << sugg << std::endl;
        if (sugg>ny) {
          ny = (unsigned long) sugg;
        }
      }
    }
  }

}

namespace std {
std::ostream & mes() {
  static std::ostream & defaultLogStream(std::cerr);
  static std::ofstream oblivionLogStream("/dev/null");
  if (Configuration::silent) {
    return oblivionLogStream;
  } else {
    return defaultLogStream;
  }
}
}


