
#ifndef LESTER_CONFIGURATION_H
#define LESTER_CONFIGURATION_H

#include <string>
#include <cairo/cairo.h>

namespace std {
std::ostream & mes();
}

class BoundMonitor;

class Configuration {
 public:
  static void hist(const unsigned int nDim, const bool isWeighted) {
    dim=nDim;
    weighted=isWeighted;
  }
  static void fromName(const std::string & s, std::ostream & err);
  static void update(const unsigned long size, const BoundMonitor & xMonitor, const BoundMonitor & yMonitor);
 public:
  typedef enum {NORMAL, UP, DOWN} YAxisLabelDirection;
  static unsigned int dim;
  static unsigned int boredomTime; // how long it takes to get bored in seconds
  static bool showHelp;
  static bool normalize;
  static bool silent;
  static bool batch;
  static bool multi;
  static bool ratio;
  static bool cumulate;
  static bool ratioConnectPoints;
  static bool showLegend;
  static std::string ratioTypeA;
  static std::string ratioTypeB;
  static bool intx;
  static bool inty;
  static bool calclx;
  static double lx;
  static bool calcux;
  static double ux;
  static bool calcly;
  static double ly;
  static bool calcuy;
  static double uy;
  static bool calclz;
  static double lz;
  static bool calcuz;
  static double uz;
  static bool calcnx;
  static unsigned int nx;
  static bool calcny;
  static unsigned int ny;
  static unsigned int SIZEX;
  static unsigned int SIZEY;
  static float borderFracLeft;
  static float borderFracRight;
  static float borderFracBot;
  static float borderFracTop;
  static float leftElbow;
  static float rightElbow;
  static float botElbow;
  static float topElbow;
  static bool weighted;
  static bool coloured;
  static bool autosizing;
  static bool markLastPoint;
  static bool makeEpsAtEnd;
  static std::string epsFileName;
  static bool makePdfAtEnd;
  static std::string pdfFileName;
  static bool makeSvgAtEnd;
  static std::string svgFileName;
  static bool makePngAtEnd;
  static std::string pngFileName;
  static bool makeDumpAtEnd;
  static std::string dumpFileName;
  static std::string xname;
  static std::string yname;
  static bool overwrite;
  static bool ignoreweights;
  static bool quitOnEnd;
  static bool noBox;
  //static bool noTitle;
  static std::string title;
  static bool haveBoxX;
  static bool haveBoxY;
  static double boxx;
  static double boxy;
  static bool drawErrors;
  static bool haveLogScaleY;
  static bool haveLogScaleZ;
  static std::string style;
  static double graphThickness;
  static double axesThickness;
  static bool fillVoid;
  static bool colourScale;
  static bool scaleWeightsToUnitIntervalWhenConvertingToColours;
  static bool centreJustify;
  static bool dark;
  static YAxisLabelDirection yAxisLabelDirection;
  static bool weKnowOurBounds() {
    return (!calclx && !calcux && (dim<2 || (!calcly && !calcuy)));
  }
  static bool outOfBoundsX(const double x) {
    return ((!calclx && x<lx-(intx?0.5:0.0)) || (!calcux && x>=ux+(intx?0.5:0.0))); // Bins are [from,to)
  }
  static bool outOfBoundsY(const double y) {
    return ((!calcly && y<ly-(inty?0.5:0.0)) || (!calcuy && y>=uy+(inty?0.5:0.0))); // Bins are [from, to)
  }
  static bool outOfBoundsXY(const double x, const double y) {
    return outOfBoundsX(x) || outOfBoundsY(y);
  }
  static void setFGColour(cairo_t * c) {
    if (Configuration::dark) {
      cairo_set_source_rgb(c, 1,1,1);
    } else {
      cairo_set_source_rgb(c, 0,0,0);
    }
  }
  static void setBGColour(cairo_t * c) {
    if (Configuration::dark) {
      cairo_set_source_rgb(c, 0,0,0);
    } else {
      cairo_set_source_rgb(c, 1,1,1);
    }
  }
};

#endif



