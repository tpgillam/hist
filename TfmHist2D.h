
#ifndef LESTER_TFMHIST2D_H
#define LESTER_TFMHIST2D_H

#include "CairoPaintable.h"
#include "TfmHistBox.h"

class Hist2D;

class TfmHist2D : public CairoPaintable {
 public:
  TfmHist2D(const Hist2D & hist);
  void paint(cairo_t * c, std::ostream & os) const;
 private:
  TfmHistBox m_tfmHistBox;
};

#endif
