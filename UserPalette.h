
#ifndef LESTER_USERPALETTE_H
#define LESTER_USERPALETTE_H

#include "InterpolatedPalette.h"
#include <memory>

class UserPalette : public InterpolatedPalette {
 private:
  UserPalette() {}
 public:
  static std::shared_ptr<UserPalette> instance();
};

#endif
