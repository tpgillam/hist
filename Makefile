INSTALL_DIR = /opt/local/bin
#INSTALL_DIR = /Users/lester/bin
#INSTALL_DIR = /usera/lester/LESTERHOME/proj/scripts



ifeq ($(shell uname -s),Darwin)
  # Directory containing libcairo.* :
  CAIRO_LIB_DIR := /opt/local/lib
  # Directory containing cairo/cairo.h :
  CAIRO_INC_DIR := /opt/local/include
else
  # Directory containing libcairo.* :
  CAIRO_LIB_DIR := /usr/lib
  # Directory containing cairo/cairo.h :
  CAIRO_INC_DIR := /usr/include
endif



export LD_RUN_PATH := $(CAIRO_LIB_DIR)




.PHONY : default clean os install

CPPFLAGS :=  -g -std=c++0x
CPPFLAGS +=  -v
CPPFLAGS += -fPIC -Wall -pthread
CPPFLAGS += -I $(CAIRO_INC_DIR) 

LDFLAGS := -std=c++0x
#LDFLAGS += -Wl,-rpath,${COMPILER_PATH}/../../../../lib -Wl,-rpath,${COMPILER_PATH}/../../../../lib64
LDFLAGS += -L $(CAIRO_LIB_DIR) -lcairo -lX11  # -gcc-mt



SRCS := $(wildcard *.c++)

OBJS := $(subst .c++,.o,$(SRCS))

default: invTest main 

install: $(INSTALL_DIR)/hist-exec
install: $(INSTALL_DIR)/hist
install: $(INSTALL_DIR)/hist2D
install: $(INSTALL_DIR)/histWeighted
install: $(INSTALL_DIR)/hist2DWeighted
install: $(INSTALL_DIR)/histRocFilter
install: $(INSTALL_DIR)/histRocFilterWeighted

$(INSTALL_DIR)/histRocFilter: histRocFilter
	mkdir -p $(INSTALL_DIR)
	cp $< $@

$(INSTALL_DIR)/histRocFilterWeighted: histRocFilterWeighted
	mkdir -p $(INSTALL_DIR)
	cp $< $@

$(INSTALL_DIR)/hist-exec: main
	mkdir -p $(INSTALL_DIR)
	cp $< $@

$(INSTALL_DIR)/hist: hist
	mkdir -p $(INSTALL_DIR)
	install $< $(INSTALL_DIR)

$(INSTALL_DIR)/histWeighted: histWeighted
	mkdir -p $(INSTALL_DIR)
	install $< $(INSTALL_DIR)

$(INSTALL_DIR)/hist2D: hist2D
	mkdir -p $(INSTALL_DIR)
	install $< $(INSTALL_DIR)

$(INSTALL_DIR)/hist2DWeighted: hist2DWeighted
	mkdir -p $(INSTALL_DIR)
	install $< $(INSTALL_DIR)


os:
	echo OS is $(OS)
	
main: $(OBJS) | invTest
	g++ -o $@ $^ ${LDFLAGS}
	@echo 
	@echo To install hist to $(INSTALL_DIR), type
	@echo
	@echo     sudo make install
	@echo	
	@echo If you want to install to some other directory, edit the Makefile first ... 

invTest: TESTS/invTest.o
	g++ -o $@ $^ ${LDFLAGS}

clean:
	rm -f $(OBJS) *~ main *.d TESTS/*.o TESTS/*.d

%.o: %.c++
	g++ -g -MP -MMD -o $@ -c $<  ${CPPFLAGS} 

include $(wildcard *.d)
include $(wildcard TESTS/*.d)

