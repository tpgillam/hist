
#include "TfmAxisText.h"
#include "TfmHistBox.h"
#include "Configuration.h"

void TfmAxisText::paint(cairo_t * c, std::ostream & os) const {
  if (c) {
    /*
  Configuration & conf = Configuration::instance();

  // reset to "default" coordinate system
  cairo_identity_matrix(c);
  
  // translate corner of histogram to desired loc
  cairo_translate(c,
		  conf.borderFracLeft*static_cast<float>(conf.SIZEX),
		  (1.-conf.borderFracBot)*static_cast<float>(conf.SIZEY));
  
  // scale histogram to fill desired box
  cairo_scale(c,
	      static_cast<float>(conf.SIZEX)*(1.-conf.borderFracLeft-conf.borderFracRight),
	      -static_cast<float>(conf.SIZEY)*(1.-conf.borderFracBot -conf.borderFracTop  ));
  

  */
  TfmHistBox tfm(0,0,1,1);
  tfm.paint(c,os);

  // patch up line width etc
  cairo_set_line_width(c,Configuration::axesThickness);
  
  cairo_matrix_t fontMat;
  cairo_matrix_init_scale(&fontMat,0.04,-0.04);
  cairo_set_font_matrix(c, &fontMat);
  }
}
