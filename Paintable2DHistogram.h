
#ifndef LESTER_PAINTABLE2DHISTOGRAM_H
#define LESTER_PAINTABLE2DHISTOGRAM_H

#include "CairoPaintableHistogram.h"
#include "Data.h"
#include <memory>

//fwd dec
class Hist2D;

class Paintable2DHistogram : public PaintableHistogram {
 public:
  Paintable2DHistogram(Data::Hist2DPtrMap hist2DPtrMap) : m_hist2DPtrMap(hist2DPtrMap) {}
  virtual void paint(cairo_t * c, std::ostream & os) const; // dump to os if c is null
 private:
  Data::Hist2DPtrMap m_hist2DPtrMap;
};

#endif
