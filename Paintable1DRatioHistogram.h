
#ifndef LESTER_PAINTABLE1DRATIOHISTOGRAM_H
#define LESTER_PAINTABLE1DRATIOHISTOGRAM_H

#include "CairoPaintableHistogram.h"
#include "Data.h"
#include <memory>

//fwd dec
class Hist1D;

class Paintable1DRatioHistogram : public PaintableHistogram {
 public: 
  Paintable1DRatioHistogram(Data::Hist1DPtrMap hist1DPtrMap) : m_hist1DPtrMap(hist1DPtrMap) {}
  virtual void paint(cairo_t * c, std::ostream & os) const; // dump to os if c is null
 private:
  Data::Hist1DPtrMap m_hist1DPtrMap;
};

#endif
