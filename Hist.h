
#ifndef LESTER_HIST_H
#define LESTER_HIST_H

#include "Configuration.h"
//#include <iostream>

class Hist {
 public:
  Hist() : minWeight(0), maxWeight(0), smallestPosWeight(0), totWeight(0) {}
  virtual ~Hist() {}
  double minWeight;
  double maxWeight;
  double smallestPosWeight; // 0 indicates not yet set
  double totWeight;
  void updateWeightBounds(const double w) {
    static int i=0;
    ++i;
    //std::cout << "Update " << i << " with w = " << w << " and totWeight = " << totWeight << std::endl;
    if (w>0 && (smallestPosWeight==0 /* not yet set*/ || w<smallestPosWeight)) {
      smallestPosWeight = w;
    }
    if (w>maxWeight) {
      maxWeight=w;
    } else if (w<minWeight) {
      minWeight=w;
    }
  }
  virtual bool normalize() = 0; // implementations if this should call "finaliseNormalize" when they complete, to patch up all weights.  Return true if normalization succeeded, and false otherwise. (Normalization is impossible if totWeight==0, for example.)
  virtual bool cumulate() { return normalize(); } // Cumulation doesn't make sense for histograms of more than 1d ... but the 1D normalisation incldues a form of normalization, so IF someone is silly enough to call cumulate on a 2D histo, we will pass this through to ordinary normalise.  See Redrawer.c++ 
 protected:
  void finaliseNormalize() {
    minWeight /= totWeight;
    maxWeight /= totWeight;
    smallestPosWeight /= totWeight;
    totWeight = 1;
  }
};

# endif

