
#ifndef LESTER_SCENE_H
#define LESTER_SCENE_H

#include "CairoPaintable.h"
#include <memory>
#include <list>

struct Scene : public CairoPaintable {
  public:
  typedef std::list<std::shared_ptr<const CairoPaintable> > CairoPaintableSharedPtrList;
  virtual void paint(cairo_t * c, std::ostream & os) const;
  public:
  inline CairoPaintableSharedPtrList & list() { return m_list; }
  inline const CairoPaintableSharedPtrList & list() const { return m_list; }
  private:
  CairoPaintableSharedPtrList m_list;
};

#endif
