
#ifndef LESTER_ALLANACHPALETTE_H
#define LESTER_ALLANACHPALETTE_H

#include "InterpolatedPalette.h"

class AllanachPalette : public InterpolatedPalette {
 public:
  AllanachPalette();
};

#endif
