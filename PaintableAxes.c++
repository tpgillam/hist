
#include "PaintableAxes.h"
#include "TfmAxisText.h"
#include "stringFrom.h"
#include "Configuration.h"

inline double safe(const double base) {
  if (base>1) {
    return base;
  } else {
    return 1+base;
  }
}

std::list<double> PaintableAxes::placesToLabel(const Axis & axis, const bool subTicks) {
  std::list<double> labels;
  const double a=axis.leftMostExtent();
  const double b=axis.rightMostExtent();

  if (a>=b) {
    return labels;
  }


  if (axis.isLogScale) {
    // log scale
    //std::cerr << "Lab start stop " << a << " " << b << std::endl;

    const double startBase=10;

    double base=startBase;

    const int maxLoops = 2000; // emergency cutoff
    int loop=0;

    bool haveTriedShrinkingBase = false;
    bool haveTriedGrowingBase = false;

    while(++loop < maxLoops) {
      labels.clear();
      // std::mes() << "LOOP " << loop << " base " << base << std::endl;
      const int maxLoops2 = 1000; // emergency cutoff
      int loop2=0;

      double lab = pow(safe(base),floor(log(a)/log(safe(base))));
      //std::cerr << "Try Lab " << lab << std::endl;
      while (lab<=b && ++loop2<maxLoops2) {
        if (subTicks) {
          labels.push_back(lab);
          const double thisLab = lab;
          const double nextLab = lab*safe(base);
          // want to put tick marks that divide space between the two labels into a fixed number of gaps,(by convention 9 gaps for base 10, 8 gaps for base 8,
          const unsigned int numGaps = 9;
          const double gapSize = (nextLab-thisLab)/numGaps;
          for (unsigned int i=1; i<=numGaps; ++i) {
            labels.push_back(lab + gapSize*i);
          }
        } else {
          labels.push_back(lab);
        }
        lab*=safe(base);
        // std::cerr << "Try Lab " << lab << std::endl;
      }

      if (labels.size()<3*(subTicks?10:1) && !haveTriedGrowingBase) {
        // too few lables -- make more by shrinking base
        base/=sqrt(10);
        haveTriedShrinkingBase=true; // ensure we don't get stuck in endless loop
      } else if (labels.size()>10*(subTicks?10:1) && !haveTriedShrinkingBase) {
        // too many labels -- make fewer by enlarging base
        base*=sqrt(10);
        haveTriedGrowingBase=true; // ensure we don't get stuck in endless loop
      } else {
        break;
      }


    }


  } else {
    // linear scale

    const double w=b-a;
    const double ten=10;
    const double l_10=log(w)/log(ten);
    const double fd=floor(l_10);
    const double fi=static_cast<int>(fd);
    double interval =pow(ten,fi);
    int n1=static_cast<int>(ceil(a/interval));
    int n2=static_cast<int>(floor(b/interval));
    int nLabs = n2-n1+1;
    while (nLabs<=2) {
      interval/=2;
      n1=static_cast<int>(ceil(a/interval));
      n2=static_cast<int>(floor(b/interval));
      nLabs = n2-n1+1;
    }
    while (nLabs>8) {
      interval*=2;
      n1=static_cast<int>(ceil(a/interval));
      n2=static_cast<int>(floor(b/interval));
      nLabs = n2-n1+1;
    }
    //std::mes() << "l_10 " << l_10 << " fd " << fd << " int " << interval << " n1 " << n1 << " n2 " << n2 << std::endl;

    for(int n=n1; n<=n2; ++n) {
      labels.push_back(n*interval);
    }

  };

  return labels;
}

void PaintableAxes::paint(cairo_t * c, std::ostream & os) const {
  if (c) {

    TfmAxisText tst;
    tst.paint(c,os);

    cairo_font_extents_t CFE;
    cairo_font_extents(c, &CFE);
    cairo_text_extents_t CTE;

    {
      // X axis

      Configuration::setFGColour(c);
      cairo_move_to(c, 0, 0);
      cairo_line_to(c, 1, 0);
      cairo_stroke(c);

      const bool justLabelEnds = false;

      if (justLabelEnds) {
        cairo_move_to(c, 0, 0-CFE.height);
        cairo_show_text(c, stringFrom(m_xAxis.leftMostExtent()).c_str());
        cairo_text_extents(c, stringFrom(m_xAxis.rightMostExtent()).c_str(), &CTE);
        cairo_move_to(c, 1-CTE.width, 0-CFE.height);
        cairo_show_text(c, stringFrom(m_xAxis.rightMostExtent()).c_str());
      } else {


        const std::list<double> & labs = placesToLabel(m_xAxis);

        const bool insertPlusses = labs.front()<0 && labs.back()>0;
        for(std::list<double>::const_iterator it = labs.begin();
            it != labs.end();
            ++it) {
          const double lab = *it;
          const std::string label = (insertPlusses && lab>0 ? std::string("+") : std::string() ) + stringFrom(lab);
          cairo_text_extents(c, label.c_str(), &CTE);
          const double xTick=(lab-m_xAxis.leftMostExtent())/(m_xAxis.rightMostExtent() - m_xAxis.leftMostExtent());
          cairo_move_to(c, xTick-CTE.width/2, 0-CFE.height);
          cairo_show_text(c, label.c_str());

          cairo_move_to(c, xTick, +0.00);
          cairo_line_to(c, xTick, -0.01);
          cairo_stroke(c);
        }


      }

      //XAxisTitle
      if (Configuration::xname!="") {
        cairo_text_extents(c, Configuration::xname.c_str(), &CTE);
        const double xpos
          = Configuration::centreJustify
            ? 0.5 - CTE.width/2
            : 1.0 - CTE.width;
        cairo_move_to(c, xpos, 0 - CFE.height*3);
        Configuration::setFGColour(c);
        cairo_show_text(c, Configuration::xname.c_str());
      }
    }

    {
      // Y axis

      Configuration::setFGColour(c);
      cairo_move_to(c, 0, 0);
      cairo_line_to(c, 0, 1);
      cairo_stroke(c);

      const bool justLabelEnds = false;


      if (justLabelEnds) {

        cairo_text_extents(c, (stringFrom(m_yAxis.leftMostExtent())+"M").c_str(), &CTE);
        cairo_move_to(c, 0-CTE.width, 0);
        cairo_show_text(c, stringFrom(m_yAxis.leftMostExtent()).c_str());
        cairo_text_extents(c, (stringFrom(m_yAxis.rightMostExtent())+"M").c_str(), &CTE);
        cairo_move_to(c, 0-CTE.width, 1);
        cairo_show_text(c, stringFrom(m_yAxis.rightMostExtent()).c_str());

      } else {

        const std::list<double> & labs = placesToLabel(m_yAxis);

        const bool insertPlusses = labs.front()<0 && labs.back()>0;
        double yTick;
        for(std::list<double>::const_iterator it = labs.begin();
            it != labs.end();
            ++it) {
          const double lab = *it;
          const std::string label = (insertPlusses && lab>0 ? std::string("+") : std::string() ) + stringFrom(lab);
          cairo_text_extents(c, (label+"M").c_str(), &CTE);
          // adapt function ...  THIS SHOULD REALLY NOT DUPLICATE THE SIMILARLY NAMED FUNCTION IN PAITABLe1DHISTORGRAM.C++ !!!! UNIFY !!!
          if (m_yAxis.isLogScale) {
            //std::cerr << "Lab pos is " << lab << std::endl;
            //yTick=static_cast<double>(rand())/RAND_MAX;//static_cast<double>(rand())/RAND_MAX;
            yTick=((log(lab)-log(m_yAxis.leftMostExtent()))/(log(m_yAxis.rightMostExtent())-log(m_yAxis.leftMostExtent())));
          } else {
            yTick =((lab-m_yAxis.leftMostExtent())/(m_yAxis.rightMostExtent() - m_yAxis.leftMostExtent())); // yTick is between 0 and 1 and is how far up we need to go
          }
          if (yTick>=0 && yTick<=1.0001) {
            cairo_move_to(c, 0-CTE.width, yTick-CTE.height/2);
            cairo_show_text(c, label.c_str());

            cairo_move_to(c, -0.01, yTick);
            cairo_line_to(c, +0.00, yTick);
            cairo_stroke(c);
          }
        }
        if (Configuration::haveLogScaleY) {
          const std::list<double> & labs = placesToLabel(m_yAxis, true);
          for(std::list<double>::const_iterator it = labs.begin();
              it != labs.end();
              ++it) {
            const double lab = *it;
            yTick=((log(lab)-log(m_yAxis.leftMostExtent()))/(log(m_yAxis.rightMostExtent())-log(m_yAxis.leftMostExtent())));
            if (yTick>=0 && yTick<=1.0001) {
              cairo_move_to(c, -0.0075, yTick);
              cairo_line_to(c, +0.00, yTick);
              cairo_stroke(c);
            }


          }
        }
      }

      //YAxisTitle
      if (Configuration::yname!="") {


        cairo_text_extents(c, Configuration::yname.c_str(), &CTE);
        const double xOff = -0.15;
        double xCen;
        double yCen;
        double phi;

        const Configuration::YAxisLabelDirection orientation = Configuration::yAxisLabelDirection;

        if (Configuration::centreJustify ) {
          // Centre justify
          if (orientation==Configuration::NORMAL) {
            phi=0;
            xCen = - CTE.width/2;
            yCen = 0.5 - CFE.height/4;
          } else if (orientation==Configuration::UP) {
            phi = -3.14159/2;
            xCen = +CFE.height/4;
            yCen = 0.5 - CTE.width/2;
          } else { //if (orientation==Configuration::DOWN) {
            phi = +3.14159/2;
            xCen = -CFE.height/4;
            yCen = 0.5 + CTE.width/2;
          }
        } else {
          // Right justify
          if (orientation==Configuration::NORMAL) {
            phi=0;
            xCen = - CTE.width/2;
            yCen = 1. - CFE.height/2;
          } else if (orientation==Configuration::UP) {
            phi = -3.14159/2;
            xCen = +CFE.height/4;
            yCen = 1. - CTE.width;
          } else { //if (orientation==Configuration::DOWN) {
            phi = +3.14159/2;
            xCen = -CFE.height/4;
            yCen = 1;
          }
        }

        cairo_save(c);
        cairo_move_to(c, xOff + xCen, yCen);
        Configuration::setFGColour(c);

        cairo_matrix_t fontMat;
        cairo_get_font_matrix(c, &fontMat);
        //cairo_matrix_init_scale(&fontMat,0.04,-0.04);
        cairo_matrix_rotate(&fontMat, phi);
        cairo_set_font_matrix(c, &fontMat);

        cairo_show_text(c, Configuration::yname.c_str());
        cairo_restore(c);
      }
    }
  }
}







